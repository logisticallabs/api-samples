﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Rest;
using Rex.Client.Events;
using Rex.Client.Jobs;

namespace RexClient_Demo
{
    public class ExecuteBulkRateSearch
    {
        private const string AvailableFields = "origin_city,origin_state,origin_zip,origin_country,destination_city,destination_state,destination_zip,destination_country,class_code,total_weight,liftgate_delivery,residential_delivery,nmfc,pallet_count,sw_rate,sw_terminal_rate,shipper_name,shipper_code";

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var loggerFactory = new LoggerFactory();
            var restClient = new RestClient("rex", loggerFactory.CreateLogger<RestClient>());
            var jobService = new JobService(restClient, loggerFactory.CreateLogger<JobService>());
            var cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
            
            Console.WriteLine("Available bulk rate search input fields: " + AvailableFields);

            var attachment = new JobAttachment
            {
                Name = "sample-data.csv",
                Content = System.IO.File.ReadAllBytes("sample-data.csv")
            };

            Console.WriteLine("Running sample bulk rate search...");
            var jobId = await jobService.StartBulkRateSearchAsync(attachment: attachment, cancellationToken: cancellationTokenSource.Token);
            Console.WriteLine("Job created. Job id: " + jobId);

            await Task.Delay(30000);    // Give some time to process bulk rate search.

            var job = await jobService.GetJobByIdAsync(jobId: jobId, cancellationToken: CancellationToken.None);

            if (job.Status == "Completed")
            {
                var resultFile = await jobService.GetBulkRateSearchResultsAsync(jobId: jobId, cancellationToken: cancellationTokenSource.Token);
                Console.WriteLine("Bulk rate search result:\n " + Encoding.UTF8.GetString(resultFile.Content));
            }
            else
            {
                Console.WriteLine("Job status is:" + job.Status);
            }
        }

        public async Task ExecuteUsingEventsAsync(CancellationToken cancellationToken)
        {
            var loggerFactory = new LoggerFactory();
            var restClient = new RestClient("rex", loggerFactory.CreateLogger<RestClient>());
            var jobService = new JobService(restClient, loggerFactory.CreateLogger<JobService>());
            var eventSource = new EventSource(restClient, loggerFactory.CreateLogger<EventSource>());
            var cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
            var tasks = new Task[2];

            var filterEventMessages = new List<string> { "job-updated" };

            Console.WriteLine("Available bulk rate search input fields: " + AvailableFields);
            
            tasks[0] = eventSource.OnListenAsync(func: async (m, c) =>
            {
                if ((string)m.Payload["JobName"] == "BulkRateSearch" && m.Payload["Status"].ToString() == "Completed")
                {
                    Console.WriteLine("Bulk rate search is completed");
                    Console.WriteLine("Bulk rate search status is:" + m.Payload["Status"]);

                    try
                    {
                        var jobId = Convert.ToInt32(m.Payload["DocumentId"]);
                        var resultFile = await jobService.GetBulkRateSearchResultsAsync(jobId: jobId, cancellationToken: cancellationTokenSource.Token);
                        Console.WriteLine("Bulk rate search result:\n " + Encoding.UTF8.GetString(resultFile.Content));
                        cancellationTokenSource.Cancel();
                    }
                    catch (RestClientException ex)
                    {
                        Console.WriteLine("Result file is unavailable: " + ex.Message);
                    }
                }
            }, eventNames: filterEventMessages, cancellationToken: cancellationTokenSource.Token);

            tasks[1] = Task.Run(async () =>
            {
                var attachment = new JobAttachment
                {
                    Name = "sample-data.csv",
                    Content = System.IO.File.ReadAllBytes("sample-data.csv")
                };

                Console.WriteLine("Running sample bulk rate search...");
                var jobId = await jobService.StartBulkRateSearchAsync(attachment: attachment, cancellationToken: cancellationTokenSource.Token);
                Console.WriteLine("Job created. Job id: " + jobId);
            }, cancellationTokenSource.Token);

            await Task.WhenAll(tasks);
        }
    }
}
