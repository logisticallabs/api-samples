﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Rest;
using Rex.Client.Events;

namespace RexClient_Demo
{
    public class ListenForTrackingInformationUpdateEvents
    {
        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var loggerFactory = new LoggerFactory();
            var restClient = new RestClient("rex", loggerFactory.CreateLogger<RestClient>());
            var eventSource = new EventSource(restClient, loggerFactory.CreateLogger<EventSource>());

            var filterEventMessages = new List<string> { "tracking-information-updated" };

            await eventSource.OnListenAsync(func: (m, c) =>
            {
                Console.WriteLine($"Load's {m.Payload["LadingNumber"]} tracking information updated {m.Payload["Status"]}");
                return Task.FromResult(0);
            }, eventNames: filterEventMessages, cancellationToken: cancellationToken);
        }
    }
}
