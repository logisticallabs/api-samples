﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Rest;
using Rex.Client.Tracking;

namespace RexClient_Demo
{
    public class FetchTrackingInformationForMultipleShipments
    {
        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var loggerFactory = new LoggerFactory();
            var restClient = new RestClient("rex", loggerFactory.CreateLogger<RestClient>());
            var trackingService = new TrackingService(restClient, loggerFactory.CreateLogger<TrackingService>());

            Console.WriteLine("Fetching multiple tracking items...");

            var trackingItems = await trackingService.GetTrackingInformationAsync(from: DateTime.Today.AddDays(-1), to: DateTime.Now, firstResult: 0, maxResults: 100, cancellationToken: cancellationToken);

            Console.WriteLine("Total tracking items fetched: " + trackingItems?.Count);
        }
    }
}
