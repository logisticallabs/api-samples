﻿using System.Reflection;

[assembly: AssemblyCompany("Logistical Labs")]
[assembly: AssemblyProduct("RexClient-Demo")]
[assembly: AssemblyCopyright("Logistical Labs © 2014-2017")]

[assembly: AssemblyVersion("1.0.0.0")]
