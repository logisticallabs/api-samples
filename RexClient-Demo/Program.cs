﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace RexClient_Demo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Logistical Labs RestClient demo");
            Console.WriteLine("Please change credentials in 'rex.config' file\n");
            Console.WriteLine("Press 1 to execute bulk rate search");
            Console.WriteLine("Press 2 to execute bulk rate search using events");
            Console.WriteLine("Press 3 to fetch multiple tracking items");
            Console.WriteLine("Press 4 to get tracking information update events");

            var choice = Convert.ToInt32(Console.ReadLine());
            var cancellationTokenSource = new CancellationTokenSource(60000);

            switch (choice)
            {
                case 1:
                    Task.Run(async () =>
                    {
                        var bulkRateSearch = new ExecuteBulkRateSearch();
                        await bulkRateSearch.ExecuteAsync(cancellationTokenSource.Token);
                    }, cancellationTokenSource.Token)
                    .Wait();
                    break;

                case 2:
                    Task.Run(async () =>
                        {
                            var bulkRateSearch = new ExecuteBulkRateSearch();
                            await bulkRateSearch.ExecuteUsingEventsAsync(cancellationTokenSource.Token);
                        }, cancellationTokenSource.Token)
                        .Wait();
                    break;

                case 3:
                    Task.Run(async () =>
                    {
                        var multipleTrackingInformation = new FetchTrackingInformationForMultipleShipments();
                        await multipleTrackingInformation.ExecuteAsync(cancellationTokenSource.Token);
                    }, cancellationTokenSource.Token)
                    .Wait();
                    break;

                case 4:
                    Task.Run(async () =>
                    {
                        var trackingInformationUpdateEvents = new ListenForTrackingInformationUpdateEvents();
                        await trackingInformationUpdateEvents.ExecuteAsync(cancellationTokenSource.Token);
                    }, cancellationTokenSource.Token)
                    .Wait();
                    break;
            }

            Console.ReadKey();
        }
    }
}
