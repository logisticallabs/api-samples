# README #

### Logistical Labs API sample projects ###

### Rex ###

* RexClient c# sample project is [in sources](https://bitbucket.org/logisticallabs/api-samples/src/3d338719e682631fcfcc204b92e5fb25c1480433/RexClient-Demo/?at=master)
* API documentation is available [here](https://logisticallabs.atlassian.net/wiki/display/PUB/RexClient)
* API reference is available [here](https://rex.logisticallabs.com/doc/)